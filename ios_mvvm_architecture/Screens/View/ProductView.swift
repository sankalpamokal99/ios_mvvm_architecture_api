//
//  HomeView.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

import SwiftUI

struct ProductView: View {
    @ObservedObject var viewModel: ProductViewModel
    
    init(viewModel: ProductViewModel = ProductViewModel()) {
        self.viewModel = viewModel
//        viewModel.fetchItems()
        viewModel.callApiUsingAsyncAwait(parameters: ["a":"b"] )
    }
    
    var body: some View {
        NavigationView {
            if viewModel.isLoading {
                ProgressView("Loading...")
                    .progressViewStyle(CircularProgressViewStyle())
                    .padding()
            } else {
                List(viewModel.items, id: \.id) { item in
                    Text(item.title)
                }
                .navigationTitle("Item List")
            }
        }.onAppear{
            debugPrint("onAppear")
        }
        .onDisappear{
            debugPrint("onDisappear")
        }
    }
    
}

struct ProductView_Previews: PreviewProvider {
    static var previews: some View {
        ProductView()
    }
}
