//
//  UserModel.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

import Foundation

struct Product: Codable {
    let id: Int
    let title: String
    let price: Double
    var description: String?
    var category: String?
    var image: String?
    var rating: Rate?
}

struct Rate: Codable {
    var rate: Double?
    var count: Int?
}
