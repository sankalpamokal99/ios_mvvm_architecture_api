//
//  ProductViewModel.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

import Foundation

final class ProductViewModel: ObservableObject {
    @Published var items: [Product] = []
    @Published var isLoading: Bool = false
    
    private let productRepository: ProductRepository
    
    init(productRepository: ProductRepository = ProductRepositoryImpl()) {
        self.productRepository = productRepository
        //        fetchItems()
    }
    
    func fetchItems() {
        //        items = [Product(id: 1, title: "Product 1", price: 10),Product(id: 2, title: "Product 2", price: 10),Product(id: 3, title: "Product 3", price: 10)]
        isLoading = true
        productRepository.getItems { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                switch result {
                case .success(let items):
                    self?.items = items
                case .failure(let error):
                    print("Error fetching items: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    func postItem(parameters: [String: Any]) async {
        isLoading = true
        do {
            // Adjust the following line based on your actual response type
            let response = try await productRepository.postItem(parameters: parameters)
            // Handle the response if needed
            print("Received response: \(response)")
        } catch {
            print("Error posting item: \(error.localizedDescription)")
        }
        isLoading = false
    }
    
    func callApiUsingAsyncAwait(parameters: [String: Any]) {
        
        Task {
            do {
                isLoading = true
                let result = try await productRepository.postItem(parameters: parameters)
                DispatchQueue.main.async {
                    switch result {
                    case .success(let response):
                        // Handle success, update UI with response
                        print(response)
                        self.items = response
                    case .failure(let error):
                        // Handle failure, show error message
                        print(error.errorMessage)
                    }
                    self.isLoading = false
                }
            }catch {
                // Handle unexpected errors
                print(error.localizedDescription)
            }
        }
    }
}
