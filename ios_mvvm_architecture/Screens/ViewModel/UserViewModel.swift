//
//  UserViewModel.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

import Foundation

class UserViewModel: ObservableObject {
    static var shared: UserViewModel = UserViewModel()
    var userList: [UserModel]  = []
    
    init(){
        fetchUserList()
    }
    
    func fetchUserList() -> Void {
        userList.append(UserModel(id: "1", name: "Sankalpa"))
        userList.append(UserModel(id: "2", name: "Suresh"))
        userList.append(UserModel(id: "3", name: "Sujata"))
        userList.append(UserModel(id: "4", name: "Mokal"))
        debugPrint("userlist created")
    }
    
    func getUserDetails(user : UserModel) -> Void{
        debugPrint("getUserDetails called")
    }
    
}
