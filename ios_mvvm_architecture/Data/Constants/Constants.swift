//
//  Constants.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

enum Constant {
    enum API {
        static let productURL = "https://fakestoreapi.com/products"
        static let baseURL = "https://fakestoreapi.com"

    }
}
