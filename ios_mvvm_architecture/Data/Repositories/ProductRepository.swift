//
//  UserRepository.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

import Foundation

protocol ProductRepository{
    func getItems(completion: @escaping (Result<[Product], Error>) -> Void)
    func addItem(parameters: [String: Any],completion: @escaping (Result<[Product], Error>) -> Void)
    
    //using async await
    func postItem(parameters: [String: Any]) async throws -> ResultModel<[Product]>
    
}
