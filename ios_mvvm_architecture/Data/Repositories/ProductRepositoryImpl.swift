//
//  UserRepositoryImpl.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

import Foundation

class ProductRepositoryImpl: ProductRepository {
    private let apiService: APIService
    
    init(apiService: APIService = APIService.shared) {
        self.apiService = apiService
    }
    
    func getItems(completion: @escaping (Result<[Product], Error>) -> Void) {
        apiService.request("/products") { (result: Result<[Product], Error>) in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func addItem(parameters: [String: Any], completion: @escaping (Result<[Product], Error>) -> Void) {
        //        let parameters = ["key": "value"]
        APIService.shared.request("/products", method: .post, parameters: parameters) { (result: Result<Product, Error>) in
            switch result {
            case .success(let response):
                print("Received response: \(response)")
            case .failure(let error):
                print("Error posting data: \(error.localizedDescription)")
            }
        }
    }
    
    
    func postItem(parameters: [String: Any]) async throws -> ResultModel<[Product]> {
        try await apiService.callApi(path: "/products", method: .post, parameters: parameters)
    }
    
    
}
