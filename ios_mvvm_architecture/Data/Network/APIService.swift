import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    // Add more HTTP methods as needed
}

enum ResultModel<T> {
    case success(T)
    case failure(ErrorModel)
}

class APIService {
    static let shared = APIService()
    
    
    func request<T: Decodable>(
        _ path: String,
        method: HTTPMethod = .get,
        parameters: [String: Any]? = nil,
        completion: @escaping (Result<T, Error>) -> Void
    ) {
        guard let url = URL(string: Constant.API.baseURL + path) else {
            completion(.failure(NSError(domain: "Invalid URL", code: 0, userInfo: nil)))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        if let parameters = parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                completion(.failure(NSError(domain: "Invalid response", code: 0, userInfo: nil)))
                return
            }
            
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let decodedData = try decoder.decode(T.self, from: data)
                    completion(.success(decodedData))
                } catch {
                    completion(.failure(error))
                }
            } else {
                completion(.failure(NSError(domain: "No data received", code: 0, userInfo: nil)))
            }
        }.resume()
    }
    
    
    //Using async await
    
    func callApi<T: Codable>(path: String,method: HTTPMethod = .get,
                             parameters: [String: Any]? = nil) async throws ->  ResultModel<T> {
        // Perform your API request here
        // Use URLSession, Alamofire, or any other networking library
        
        // Example using URLSession
        guard let url = URL(string: Constant.API.baseURL + path) else { return .failure(ErrorModel(errorMessage: "InvalidURL")) }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        if let parameters = parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            
            let decodedData = try JSONDecoder().decode(T.self, from: data)
            debugPrint("Response: \(decodedData)")

            return .success(decodedData)
        } catch {
            debugPrint("Error: \(error.localizedDescription)")
            return .failure(ErrorModel(errorMessage: error.localizedDescription))
        }
    }
}

