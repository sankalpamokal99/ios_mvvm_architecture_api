//
//  AppLifecycleManager.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 12/12/23.
//

import Foundation

final class AppLifeCycleManager{
    var state: AppPhase = .none
    public static var instance  = AppLifeCycleManager()
    
    private init() {
    }
    
    enum AppPhase{
        case active
        case inactive
        case background
        case none
    }
}
