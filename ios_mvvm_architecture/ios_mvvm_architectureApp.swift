//
//  ios_mvvm_architectureApp.swift
//  ios_mvvm_architecture
//
//  Created by Sankalpa Mokal on 11/12/23.
//

import SwiftUI

@main
struct ios_mvvm_architectureApp: App {
    
    @Environment(\.scenePhase) private var scenePhase
    
    var body: some Scene {
        WindowGroup {
            ProductView()
        } .onChange(of: scenePhase) { newScenePhase in
            switch newScenePhase {
            case .active:
                debugPrint("App is active")
                AppLifeCycleManager.instance.state  = .active
            case .inactive:
                debugPrint("App is inactive")
                AppLifeCycleManager.instance.state  = .inactive
            case .background:
                debugPrint("App is in background")
                AppLifeCycleManager.instance.state  = .background
            @unknown default:
                debugPrint("Interesting: Unexpected new value.")
                AppLifeCycleManager.instance.state  = .none
            }
        }
    }
}
